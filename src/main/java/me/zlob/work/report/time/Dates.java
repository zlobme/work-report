package me.zlob.work.report.time;

import java.time.*;
import java.util.*;

public class Dates {

    public static LocalDate min(LocalDate first, LocalDate second) {
        return first.isAfter(second) ? second : first;
    }

    public static LocalDate max(LocalDate first, LocalDate second) {
        return first.isBefore(second) ? second : first;
    }

    public static LocalDateTime min(LocalDateTime first, LocalDateTime second) {
        return first.isAfter(second) ? second : first;
    }

    public static LocalDateTime max(LocalDateTime first, LocalDateTime second) {
        return first.isBefore(second) ? second : first;
    }

    public static Date asDate(LocalDate localDate) {
        Instant instant = localDate.atStartOfDay()
            .atZone(ZoneId.systemDefault())
            .toInstant();
        return Date.from(instant);
    }

    public static Date asDate(LocalDateTime localDateTime) {
        Instant instant = localDateTime
            .atZone(ZoneId.systemDefault())
            .toInstant();
        return Date.from(instant);
    }

}
