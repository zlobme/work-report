package me.zlob.work.report.time;

import java.time.*;
import java.util.*;
import java.util.function.*;

import static me.zlob.util.Conditions.*;

public class Period {

    private static final Set<DayOfWeek> HOLIDAYS;

    static {
        HOLIDAYS = Set.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
    }

    private LocalDate start;

    private LocalDate end;

    public Period(LocalDate start, LocalDate end) {
        this.start = checkNotNull(start);
        this.end = checkNotNull(end);
    }

    public int getWeeks() {
        long days = start.datesUntil(end.plusDays(1)).count();
        return (int) (days / 7);
    }

    public Duration getWorkTime() {
        long workDays = start.datesUntil(end.plusDays(1))
            .map(LocalDate::getDayOfWeek)
            .filter(Predicate.not(HOLIDAYS::contains))
            .count();
        return Duration.ofHours(workDays * 8);
    }

    //<editor-fold desc="Getters">
    public LocalDate getStart() {
        return start;
    }

    public LocalDate getEnd() {
        return end;
    }
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Period period = (Period) o;
        return Objects.equals(start, period.start) &&
               Objects.equals(end, period.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public String toString() {
        return String.format("[%s, %s]", start, end);
    }
    //</editor-fold>

}
