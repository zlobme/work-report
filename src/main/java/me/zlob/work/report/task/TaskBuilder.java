package me.zlob.work.report.task;

import java.time.*;

public class TaskBuilder {

    private TaskKey key;

    private TaskType type;

    private LocalDateTime start;

    private LocalDateTime end;

    private Duration duration;

    public TaskBuilder setKey(TaskKey key) {
        this.key = key;
        return this;
    }

    public TaskBuilder setType(TaskType type) {
        this.type = type;
        return this;
    }

    public TaskBuilder setStart(LocalDateTime start) {
        this.start = start;
        return this;
    }

    public TaskBuilder setEnd(LocalDateTime end) {
        this.end = end;
        return this;
    }

    public TaskBuilder setDuration(Duration duration) {
        this.duration = duration;
        return this;
    }

    public TaskEntry buildEntry() {
        return new TaskEntry(key, type, start, end, duration);
    }

    public TaskSummary buildSummary() {
        return new TaskSummary(key, type, start, end, duration);
    }

}
