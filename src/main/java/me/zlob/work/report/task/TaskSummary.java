package me.zlob.work.report.task;

import java.time.*;

public class TaskSummary extends TaskEntry {

    public TaskSummary(
        TaskKey key,
        TaskType type,
        LocalDateTime start,
        LocalDateTime end,
        Duration duration
    ) {
        super(key, type, start, end, duration);
    }

}
