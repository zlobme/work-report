package me.zlob.work.report.task;

import java.time.*;

import lombok.*;

import static me.zlob.util.Conditions.*;

public class TaskEntry {

    @Getter
    private TaskKey key;

    @Getter @Setter
    private TaskType type;

    @Getter @Setter
    private LocalDateTime start;

    @Getter @Setter
    private LocalDateTime end;

    @Getter @Setter
    private Duration duration;

    public TaskEntry(
        TaskKey key,
        TaskType type,
        LocalDateTime start,
        LocalDateTime end,
        Duration duration
    ) {
        this.key = checkNotNull(key);
        this.type = checkNotNull(type);
        this.start = checkNotNull(start);
        this.end = checkNotNull(end);
        this.duration = checkNotNull(duration);
    }

}
