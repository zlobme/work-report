package me.zlob.work.report.task.collections;

import java.util.*;

import me.zlob.collections.*;
import me.zlob.functional.*;
import me.zlob.work.report.task.*;

public class TasksByKeys extends DelegatedMap<TaskKey, TaskEntry> {

    public static TasksByKeys of(List<TaskEntry> tasks) {
        Map<TaskKey, TaskEntry> collect = tasks.stream()
            .map(task -> Tuple.of(task.getKey(), task))
            .collect(Containers.toMap());
        return new TasksByKeys(collect);
    }

    public TasksByKeys() {
    }

    public TasksByKeys(Map<TaskKey, TaskEntry> origin) {
        super(origin);
    }

}
