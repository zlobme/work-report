package me.zlob.work.report.task;

import java.util.*;

import static me.zlob.util.Conditions.*;

public class TaskType {

    private String type;

    public static TaskType of(String type) {
        return new TaskType(existsNotEmpty(type) ? type : "unknown");
    }

    public TaskType(String type) {
        this.type = checkNotNull(type);
    }

    //<editor-fold desc="Object override">
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        TaskType taskType = (TaskType) o;
        return Objects.equals(type, taskType.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    @Override
    public String toString() {
        return type;
    }
    //</editor-fold>

}
