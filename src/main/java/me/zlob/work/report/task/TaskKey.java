package me.zlob.work.report.task;

import java.util.*;

import static me.zlob.util.Conditions.*;

public class TaskKey {

    private String key;

    public static TaskKey of(String key) {
        return new TaskKey(existsNotEmpty(key) ? key : "unknown");
    }

    public TaskKey(String key) {
        this.key = checkNotNull(key);
    }

    //<editor-fold desc="Object override">
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        TaskKey taskKey = (TaskKey) o;
        return Objects.equals(key, taskKey.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public String toString() {
        return key;
    }
    //</editor-fold>

}
