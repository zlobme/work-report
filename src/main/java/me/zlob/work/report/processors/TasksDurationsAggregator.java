package me.zlob.work.report.processors;

import java.time.*;
import java.util.function.*;

import me.zlob.work.report.task.*;
import me.zlob.work.report.task.collections.*;

public class TasksDurationsAggregator implements Consumer<TaskEntry>, Supplier<TasksDurations> {

    private TasksDurations aggregation = new TasksDurations();

    @Override
    public void accept(TaskEntry task) {
        aggregation.putIfAbsent(task.getKey(), Duration.ZERO);
        aggregation.merge(task.getKey(), task.getDuration(), Duration::plus);
    }

    @Override
    public TasksDurations get() {
        return aggregation;
    }

}
