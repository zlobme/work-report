package me.zlob.work.report.processors;

import java.time.*;
import java.util.function.*;

import me.zlob.work.report.task.*;
import me.zlob.work.report.task.collections.*;

import static me.zlob.work.report.time.Dates.*;

public class TasksByDaysAggregator implements Consumer<TaskEntry>, Supplier<TasksByDays> {

    private TasksByDays taskByDays = new TasksByDays();

    @Override
    public void accept(TaskEntry task) {
        LocalDate taskStart = task.getStart().toLocalDate();

        taskByDays.computeIfAbsent(taskStart, k -> new TasksByKeys());
        TasksByKeys dayTasks = taskByDays.get(taskStart);

        dayTasks.computeIfAbsent(task.getKey(), k -> duplicateEmpty(task));
        dayTasks.merge(task.getKey(), task, this::mergeTaskEntries);
    }

    @Override
    public TasksByDays get() {
        return taskByDays;
    }

    private TaskEntry duplicateEmpty(TaskEntry task) {
        return new TaskBuilder()
            .setKey(task.getKey())
            .setType(task.getType())
            .setStart(task.getStart())
            .setEnd(task.getStart())
            .setDuration(Duration.ZERO)
            .buildEntry();
    }

    private TaskEntry mergeTaskEntries(TaskEntry accumulator, TaskEntry task) {
        accumulator.setStart(min(accumulator.getStart(), task.getStart()));
        accumulator.setEnd(max(accumulator.getStart(), task.getStart()));
        accumulator.setDuration(accumulator.getDuration().plus(task.getDuration()));
        return accumulator;
    }

}
