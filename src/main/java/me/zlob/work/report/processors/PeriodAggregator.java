package me.zlob.work.report.processors;

import java.time.*;
import java.util.function.*;

import me.zlob.work.report.task.*;
import me.zlob.work.report.time.*;
import me.zlob.work.report.time.Period;

import static me.zlob.util.Conditions.*;

public class PeriodAggregator implements Consumer<TaskEntry>, Supplier<Period> {

    private static final DayOfWeek WEEK_START = DayOfWeek.MONDAY;

    private LocalDate periodStart;

    private LocalDate periodEnd;

    public PeriodAggregator() {
        this(LocalDate.MAX, LocalDate.MIN);
    }

    public PeriodAggregator(
        LocalDate periodStart,
        LocalDate periodEnd
    ) {
        this.periodStart = checkNotNull(periodStart);
        this.periodEnd = checkNotNull(periodEnd);
    }

    @Override
    public void accept(TaskEntry task) {
        LocalDate taskStart = task.getStart().toLocalDate();
        periodStart = Dates.min(periodStart, taskStart);

        LocalDate taskEnd = task.getEnd().toLocalDate();
        periodEnd = Dates.max(periodEnd, taskEnd);
    }

    @Override
    public Period get() {
        LocalDate adjustedStart = periodStart.with(WEEK_START);
        LocalDate adjustedEnd = periodEnd.with(WEEK_START.minus(1));
        return new Period(adjustedStart, adjustedEnd);
    }

}
