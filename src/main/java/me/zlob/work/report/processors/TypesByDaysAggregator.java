package me.zlob.work.report.processors;

import java.time.*;
import java.util.function.*;

import me.zlob.work.report.task.*;
import me.zlob.work.report.task.collections.*;

public class TypesByDaysAggregator implements Consumer<TaskEntry>, Supplier<TypesByDays> {

    private final TypesByDays typesByDays = new TypesByDays();

    @Override
    public void accept(TaskEntry task) {
        LocalDate taskStart = task.getStart().toLocalDate();

        typesByDays.computeIfAbsent(taskStart, k -> new TypesDurations());
        TypesDurations dayTypes = typesByDays.get(taskStart);

        dayTypes.putIfAbsent(task.getType(), Duration.ZERO);
        dayTypes.merge(task.getType(), task.getDuration(), Duration::plus);
    }

    @Override
    public TypesByDays get() {
        return typesByDays;
    }

}
