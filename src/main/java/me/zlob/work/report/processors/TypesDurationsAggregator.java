package me.zlob.work.report.processors;

import java.time.*;
import java.util.*;
import java.util.function.*;

import me.zlob.work.report.task.*;

public class TypesDurationsAggregator implements Consumer<TaskEntry>, Supplier<Map<TaskType, Duration>> {

    private Map<TaskType, Duration> aggregation = new HashMap<>();

    @Override
    public void accept(TaskEntry taskEntry) {
        aggregation.putIfAbsent(taskEntry.getType(), Duration.ZERO);
        aggregation.merge(taskEntry.getType(), taskEntry.getDuration(), Duration::plus);
    }

    @Override
    public Map<TaskType, Duration> get() {
        return aggregation;
    }

}
