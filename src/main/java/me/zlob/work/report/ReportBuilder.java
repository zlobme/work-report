package me.zlob.work.report;

import java.time.*;
import java.util.*;

import me.zlob.work.report.processors.*;
import me.zlob.work.report.task.*;
import me.zlob.work.report.task.collections.*;
import me.zlob.work.report.time.*;
import me.zlob.work.report.time.Period;

import static me.zlob.util.Conditions.*;

public class ReportBuilder {

    private List<TaskEntry> tasks;

    public ReportBuilder(List<TaskEntry> tasks) {
        this.tasks = checkNotNull(tasks);
    }

    public Map<String, Object> getReportModel() {
        var periodAggregator = new PeriodAggregator();
        var tasksByDaysAggregator = new TasksByDaysAggregator();
        var tasksDurationsAggregator = new TasksDurationsAggregator();
        var typesByDaysAggregator = new TypesByDaysAggregator();
        var typesDurationsAggregator = new TypesDurationsAggregator();

        Duration totalDuration = tasks.stream()
            .peek(periodAggregator)
            .peek(tasksByDaysAggregator)
            .peek(tasksDurationsAggregator)
            .peek(typesByDaysAggregator)
            .peek(typesDurationsAggregator)
            .map(TaskEntry::getDuration)
            .reduce(Duration.ZERO, Duration::plus);

        Period period = periodAggregator.get();

        Map<String, Object> result = new LinkedHashMap<>();
        result.put("tasks", TasksByKeys.of(tasks));
        result.put("period_start", Dates.asDate(period.getStart()));
        result.put("period_end", Dates.asDate(period.getEnd()));
        result.put("period_weeks", period.getWeeks());
        result.put("period_work_time", formatDuration(period.getWorkTime()));
        result.put("tasks_by_days", tasksByDaysAggregator.get());
        result.put("tasks_durations", tasksDurationsAggregator.get());
        result.put("types_by_days", typesByDaysAggregator.get());
        result.put("types_durations", typesDurationsAggregator.get());
        result.put("total_duration", formatDuration(totalDuration));
        return result;
    }

    private static String formatDuration(Duration duration) {
        long hours = duration.toHours();
        long minutes = duration.minusHours(hours).toMinutes();
        return String.format("%02d:%02d", hours, minutes);
    }

}
