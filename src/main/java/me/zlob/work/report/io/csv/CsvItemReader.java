package me.zlob.work.report.io.csv;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import org.supercsv.io.*;
import org.supercsv.prefs.*;

import me.zlob.collections.*;
import me.zlob.work.report.io.*;

public class CsvItemReader implements ItemReader {

    private final Reader dataReader;

    private final CsvPreference csvPreference;

    private CsvMapReader csvReader;

    public CsvItemReader(Reader dataReader, CsvPreference csvPreference) {
        this.dataReader = Objects.requireNonNull(dataReader);
        this.csvPreference = Objects.requireNonNull(csvPreference);
    }

    @Override
    public List<RawItem> read() throws UncheckedIOException {
        return CsvReaderIterator.of(getCsvReader()).stream()
            .map(this::filterEmptyValues)
            .map(RawItem::new)
            .collect(Collectors.toList());
    }

    private Map<String, String> filterEmptyValues(Map<String, String> item) {
        return item.entrySet().stream()
            .filter(entry -> entry.getValue() != null)
            .collect(Containers.toMap());
    }

    private CsvMapReader getCsvReader() {
        if (csvReader == null) {
            csvReader = new CsvMapReader(dataReader, csvPreference);
        }
        return csvReader;
    }

}
