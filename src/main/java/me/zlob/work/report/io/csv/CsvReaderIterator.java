package me.zlob.work.report.io.csv;

import java.util.*;
import java.util.stream.*;

import org.supercsv.io.*;

import static me.zlob.util.Exceptions.*;

public class CsvReaderIterator
    implements Iterable<Map<String, String>>, Iterator<Map<String, String>> {

    private final CsvMapReader reader;

    private final String[] headers;

    private Map<String, String> currentItem;

    public static CsvReaderIterator of(CsvMapReader reader) {
        String[] header = rethrow(() -> reader.getHeader(true));
        return new CsvReaderIterator(reader, header);
    }

    public CsvReaderIterator(CsvMapReader reader, String[] headers) {
        this.reader = reader;
        this.headers = headers;
    }

    public Stream<Map<String, String>> stream() {
        return StreamSupport.stream(this.spliterator(), false);
    }

    @Override
    public boolean hasNext() {
        currentItem = readNext(headers);
        return currentItem != null;
    }

    @Override
    public Map<String, String> next() {
        return currentItem;
    }

    private Map<String, String> readNext(String[] headers) {
        return rethrow(() -> reader.read(headers));
    }

    @Override
    public Iterator<Map<String, String>> iterator() {
        return this;
    }

}
