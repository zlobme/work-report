package me.zlob.work.report.io;

import java.io.*;
import java.time.*;
import java.util.*;
import java.util.stream.*;

import static java.time.LocalTime.*;
import static java.time.format.DateTimeFormatter.*;

import me.zlob.work.report.task.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.work.Errors.*;

public class ToggleTaskEntryLoader implements TaskEntryLoader {

    private ItemReader itemReader;

    public ToggleTaskEntryLoader(ItemReader itemReader) {
        this.itemReader = checkNotNull(itemReader);
    }

    @Override
    public List<TaskEntry> load() {
        return readRawItems().stream()
            .map(this::convert)
            .map(this::validateDuration)
            .collect(Collectors.toList());
    }

    private List<RawItem> readRawItems() {
        try {
            return itemReader.read();
        } catch (UncheckedIOException ex) {
            String message = ITEMS_LOAD_ERROR.getMessage();
            throw new IllegalStateException(message, ex);
        }
    }

    private TaskEntry convert(RawItem rawItem) {
        return new TaskBuilder()
            .setKey(TaskKey.of(rawItem.getOrDefault("Description", "unknown")))
            .setType(TaskType.of(rawItem.getOrDefault("Tags", "unknown")))
            .setStart(parseStartDateTime(rawItem))
            .setEnd(parseEndDateTime(rawItem))
            .setDuration(parseDuration(rawItem.getOrDefault("Duration", "")))
            .buildEntry();
    }

    private TaskEntry validateDuration(TaskEntry item) {
        Duration duration = Duration.between(item.getStart(), item.getEnd());
        if (!duration.equals(item.getDuration())) {
            String message = DURATIONS_NOT_EQ_ERROR.getMessage(
                item.getKey(),
                item.getStart(),
                item.getDuration(),
                duration);
            throw new IllegalStateException(message);
        }
        return item;
    }

    private LocalDateTime parseStartDateTime(RawItem rawItem) {
        return parseDateTime(rawItem.get("Start date"), rawItem.get("Start time"));
    }

    private LocalDateTime parseEndDateTime(RawItem rawItem) {
        return parseDateTime(rawItem.get("End date"), rawItem.get("End time"));
    }

    private LocalDateTime parseDateTime(String date, String time) {
        return LocalDateTime.parse(date + "T" + time, ISO_LOCAL_DATE_TIME);
    }

    private Duration parseDuration(String duration) {
        Duration result = Duration.ZERO;
        if (existsNotEmpty(duration)) {
            LocalTime time = parse(duration, ISO_LOCAL_TIME);
            int seconds = time.toSecondOfDay();
            result = Duration.ofSeconds(seconds);
        }
        return result;
    }

}
