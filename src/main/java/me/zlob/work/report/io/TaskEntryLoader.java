package me.zlob.work.report.io;

import java.util.*;

import me.zlob.work.report.task.*;

public interface TaskEntryLoader {

    public List<TaskEntry> load();

}
