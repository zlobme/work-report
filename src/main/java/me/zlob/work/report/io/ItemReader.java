package me.zlob.work.report.io;

import java.io.*;
import java.util.*;

public interface ItemReader {

    public List<RawItem> read() throws UncheckedIOException;

}
