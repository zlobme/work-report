package me.zlob.work;

public enum Errors {

    DURATIONS_NOT_EQ_ERROR("Durations of item %s at %s not match: expected %s, actual %s"),
    ITEMS_LOAD_ERROR("Error with loading items."),
    NO_DATA_FILE_ERROR("No data file at '%s'"),
    UNABLE_WRITE_ERROR("Unable write to file %s"),
    ;

    private String message;

    Errors(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getMessage(Object... args) {
        return String.format(message, args);
    }

}
