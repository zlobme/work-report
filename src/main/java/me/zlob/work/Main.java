package me.zlob.work;

import java.io.*;
import java.util.*;

import static java.nio.charset.StandardCharsets.*;

import freemarker.template.*;

import static org.supercsv.prefs.CsvPreference.*;

import me.zlob.util.*;
import me.zlob.work.report.*;
import me.zlob.work.report.io.*;
import me.zlob.work.report.io.csv.*;
import me.zlob.work.report.task.*;

import static me.zlob.util.Conditions.*;
import static me.zlob.util.Exceptions.*;

public class Main {

    public static void main(String[] args) {
        if (notExistsOrEmpty(args)) {
            console("No data to process... exit");
            return;
        }

        try {
            InputStream input = args.length > 0
                ? getFileInput(args[0])
                : System.in;

            OutputStream output = args.length > 1
                ? getFileOutput(args[1])
                : System.out;

            if (input == null) {
                console("Data file not exists: %s", args[0]);
                return;
            }

            TaskEntryLoader loader = createItemLoader(input);
            List<TaskEntry> taskEntries = loader.load();

            ReportBuilder reportBuilder = new ReportBuilder(taskEntries);
            Map<String, Object> reportModel = reportBuilder.getReportModel();

            TemplateProcessor processor = createTemplateProcessor();
            String templateContent = getTemplate("report.ftl");
            Template template = processor.create("report.ftl", templateContent);

            String reportView = processor.process(template, reportModel);

            PrintWriter writer = createReportWriter(output);
            writer.print(reportView);
            writer.close();
        } catch (Exception ex) {
            console(ex.getMessage());
            error(ex, ex.getMessage());
        }
    }

    private static InputStream getFileInput(String fileName) {
        InputStream input = null;
        File file = new File(fileName);
        if (IO.exists(file)) {
            console("Process time report data from %s", fileName);
            input = createFileInput(file);
        }
        return input;
    }

    private static InputStream createFileInput(File dataFile) {
        try {
            return new FileInputStream(dataFile);
        } catch (FileNotFoundException ex) {
            String message = Errors.NO_DATA_FILE_ERROR.getMessage(dataFile);
            throw new IllegalStateException(message, ex);
        }
    }

    private static OutputStream getFileOutput(String fileName) {
        File file = new File(fileName);
        console("Write report to %s", fileName);
        return createFileOutput(file);
    }

    private static OutputStream createFileOutput(File file) {
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            String message = Errors.UNABLE_WRITE_ERROR.getMessage(file.getPath());
            throw createIllegalState(message, ex);
        }
    }

    private static TaskEntryLoader createItemLoader(InputStream input) {
        Reader reader = new InputStreamReader(input, UTF_8);
        ItemReader itemReader = new CsvItemReader(reader, EXCEL_PREFERENCE);
        return new ToggleTaskEntryLoader(itemReader);
    }

    private static PrintWriter createReportWriter(OutputStream reportStream) {
        return new PrintWriter(new OutputStreamWriter(reportStream, UTF_8));
    }

    private static TemplateProcessor createTemplateProcessor() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_23);
        configuration.setDateFormat(Time.DATE_PATTERN);
        configuration.setDateTimeFormat(Time.DATE_TIME_PATTERN);
        TemplateProcessor processor = new TemplateProcessor(configuration, "utf-8");
        return processor;
    }

    private static String getTemplate(String name) {
        InputStream templateInput = loadResource(name);
        String result = IO.read(templateInput);
        IO.close(templateInput);
        return result;
    }

    private static InputStream loadResource(String name) {
        return ClassLoader.getSystemClassLoader()
            .getResourceAsStream(name);
    }

    private static void console(String message, Object... args) {
        System.out.println(String.format(message, args));
    }

    private static void error(Exception ex, String message, Object... args) {
        System.err.println(String.format(message, args));
        ex.printStackTrace(System.err);
    }

}
