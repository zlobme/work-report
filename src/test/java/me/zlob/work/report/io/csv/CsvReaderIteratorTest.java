package me.zlob.work.report.io.csv;

import java.io.*;

import static java.lang.ClassLoader.*;

import org.supercsv.io.*;
import org.supercsv.prefs.*;

import org.junit.*;
import static org.junit.Assert.*;

public class CsvReaderIteratorTest {

    private static final String testFile = "toggle-test.csv";

    @Test
    public void read_sample_data() {
        Reader reader = new InputStreamReader(getResource(testFile));
        CsvMapReader csvReader = new CsvMapReader(reader, CsvPreference.EXCEL_PREFERENCE);

        CsvReaderIterator iterator = CsvReaderIterator.of(csvReader);

        assertEquals(43, iterator.stream().count());
    }

    private InputStream getResource(String file) {
        ClassLoader classLoader = getSystemClassLoader();
        return classLoader.getResourceAsStream(file);
    }

}
